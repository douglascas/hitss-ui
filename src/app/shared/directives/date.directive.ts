import { Directive, HostListener } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
	selector: '[dateMask]',
})
export class DateMaskDirective {

	constructor(public ngControl: NgControl) { }

	@HostListener('ngModelChange', ['$event'])
	onModelChange(event) {
		this.onInputChange(event, false);
	}

	@HostListener('keydown.backspace', ['$event'])
	keydownBackspace(event) {
		this.onInputChange(event.target.value, true);
	}

	onInputChange(event, backspace) {
		if (typeof event === 'string') {
			let date = event ? event.replace(/\D/g, '') : '';
			if (backspace && date.length <= 6) {
				date = date.substring(0, date.length - 1);
			}
			if (!date.length) {
				date = null;
			} else if (date.length <= 3) {
				date = date.replace(/^(\d{0,2})(\d{0,2})/, '$1/$2');
			} else if (date.length <= 4) {
				date = date.replace(/^(\d{0,2})(\d{0,2})/, '$1/$2');
			} else if (date.length <= 6) {
				date = date.replace(/^(\d{0,2})(\d{0,2})(\d{0,2})/, '$1/$2/$3');
			} else if (date.length >= 7) {
				date = date.replace(/^(\d{0,2})(\d{0,2})(\d{0,4})/, '$1/$2/$3');
			}

			this.ngControl.valueAccessor.writeValue(date);
		}
	}

}
