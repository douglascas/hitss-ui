import { Injectable } from '@angular/core';

export enum RegisterType {
  Modal = 'MODAL',
  Route = 'ROUTE'
}

@Injectable()
export class Globals {
  config = {
    configurationRegistrationParams: {
      registerType: RegisterType.Route
    },
    configurationJSONExibition: {
      showJSONPreview: true,
    }
  }
}

export class PreferenceConfigParams {
  registerType: RegisterType;
  showJSONPreview: boolean;

  constructor(init?: Partial<PreferenceConfigParams>) {
    if (init) {
      Object.assign(this, init);
    }
  }

}
