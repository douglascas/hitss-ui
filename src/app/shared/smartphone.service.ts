import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SmartphoneModel } from './smartphone-model';

@Injectable({ providedIn: 'root' })
export class SmartphoneService {

  constructor(private readonly _http: HttpClient) { }

  getSmartphones(): Observable<SmartphoneModel[]> {
    return this._http.get<SmartphoneModel[]>('http://localhost:3000/smartphones');
  }

  getSmartphoneModelsById(smartphoneId: number): Observable<SmartphoneModel[]> {
    const url = `http://localhost:3000/smartphones?id=${smartphoneId.toString()}`;

    // if (param && param.analizysType.length) {
    //   return this._http.get<SmartphoneModel[]>(url, {
    //     params: new HttpParams().set('analizysType', String(param.analizysType).toLocaleUpperCase())
    //   });
    // } else {
    // }
    return this._http.get<SmartphoneModel[]>(url);

  }

  createSmartphone(smartphoneModel: SmartphoneModel): Observable<SmartphoneModel> {
    const url = 'http://localhost:3000/smartphones';
    return this._http.post<SmartphoneModel>(url, smartphoneModel);
  }

  updateSmartphone(smartphoneModel: SmartphoneModel): Observable<SmartphoneModel> {
    const url = 'http://localhost:3000/smartphones';
    return this._http.put<SmartphoneModel>(`${url}/${smartphoneModel.id}`, smartphoneModel);
  }

  deleteSmartphone(smartphoneModel: SmartphoneModel): Observable<void> {
    const url = 'http://localhost:3000/smartphones';
    return this._http.delete<void>(`${url}/${smartphoneModel.id}`);
  }

}
