export enum SmartphoneColors {
  Black = 'BLACK',
  Gold = 'GOLD',
  Pink = 'PINK',
  White = 'WHITE'
}

export class SmartphoneModel {

  /**
   * Identificador do smartphone
   * FIXME: Mudar para `code`
   */
  id: number;

  /**
   * Modelo do smartphone
   */
  model: string;

  /**
   * Preço do smartphone
   */
  price: number;

  /**
   * Marca do smartphone
   */
  brand: string;

  /**
   * Foto do smartphone
   */
  photo: string;

  /**
   * Data de início das vendas do smartphone
   */
  startDate: string;

  /**
   * Data do fim das vendas do smartphone
   */
  endDate: string;

  /**
   * Cor do smartphone
   */
  color: SmartphoneColors;

  constructor(init?: Partial<SmartphoneModel>) {
    if (init) {
      Object.assign(this, init);
    }
  }
}
