import { Component } from '@angular/core';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ConfigurationModalComponent } from './configuration-modal';
import { ConfigurationModalService } from './configuration-modal/configuration-modal.service';
import { Globals } from './shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'hitss-project-ui';
  modalRef: NgbModalRef;

  constructor(
    globals: Globals,
    private readonly _toastr: ToastrService,
    private readonly _modalService: NgbModal,
    private readonly _configService: ConfigurationModalService
  ) {
    this._configService.getConfig()
      .subscribe(config => {
        globals.config.configurationJSONExibition.showJSONPreview = config.showJSONPreview;
        globals.config.configurationRegistrationParams.registerType = config.registerType;
      });
  }

  openConfigModal(): void {
    this.modalRef = this._modalService
      .open(ConfigurationModalComponent, {
        keyboard: false,
      });
  }
}
