import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PreferenceConfigParams } from '../shared';

@Injectable({ providedIn: 'root' })
export class ConfigurationModalService {
  constructor(private readonly _http: HttpClient) { }

  getConfig(): Observable<any> {
    const url = 'http://localhost:3000/config';
    return this._http.get(url);
  }

  updateConfig(config: PreferenceConfigParams): Observable<any> {
    const url = 'http://localhost:3000/config';
    return this._http.put(url, config);
  }
}