import { Component, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Globals, PreferenceConfigParams } from '../shared';
import { ConfigurationModalService } from './configuration-modal.service';

@Component({
  selector: 'app-configuration-modal',
  templateUrl: './configuration-modal.component.html',
  styleUrls: ['./configuration-modal.component.scss']
})
export class ConfigurationModalComponent implements OnInit {

  preferenceConfig = new PreferenceConfigParams();

  constructor(
    public globals: Globals,
    public activeModal: NgbActiveModal,
    private readonly _toastr: ToastrService,
    private readonly _configService: ConfigurationModalService,
    public config: NgbModalConfig,
  ) { }

  ngOnInit(): void {
    this._configService.getConfig()
      .subscribe(config => this.preferenceConfig = config);
  }

  close(): void {
    this.activeModal.close();
  }

  saveConfig(): void {
    const config = new PreferenceConfigParams({
      registerType: this.preferenceConfig.registerType,
      showJSONPreview: this.preferenceConfig.showJSONPreview
    });

    this._configService.updateConfig(config)
      .subscribe(() => {
        this.globals.config.configurationJSONExibition.showJSONPreview = config.showJSONPreview;
        this.globals.config.configurationRegistrationParams.registerType = config.registerType;
        this._toastr.success('Configurações alteradas com sucesso.');
        this.close();
      });
  }

}
