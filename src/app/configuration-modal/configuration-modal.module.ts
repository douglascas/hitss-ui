import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { ConfigurationModalComponent } from './configuration-modal.component';

@NgModule({
  imports: [FormsModule, SharedModule],
  exports: [ConfigurationModalComponent],
  declarations: [ConfigurationModalComponent],
  entryComponents: [ConfigurationModalComponent],
})
export class ConfigurationModalModule { }
