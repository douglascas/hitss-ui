import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Globals, SmartphoneModel } from '../../../shared';

@Component({
  selector: 'app-smartphone-form',
  templateUrl: 'smartphone-form.component.html',
  styleUrls: ['smartphone-form.component.scss']
})
export class SmartphoneFormComponent implements OnInit {

  @Input() smartphone: SmartphoneModel;
  @ViewChild('smartphoneForm') smartphoneForm: NgForm;

  constructor(public globals: Globals) { }

  ngOnInit() { }

}