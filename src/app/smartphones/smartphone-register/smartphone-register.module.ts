import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { CreateModalComponent } from './create-smartphone-modal/create-modal.component';
import { SmartphoneFormComponent } from './smartphone-form/smartphone-form.component';
import { SmartphoneRegisterPageComponent } from './smartphone-register-page/smartphone-register-page.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ],
  exports: [
    CreateModalComponent,
    SmartphoneFormComponent,
    SmartphoneRegisterPageComponent
  ],
  declarations: [
    CreateModalComponent,
    SmartphoneFormComponent,
    SmartphoneRegisterPageComponent
  ],
  entryComponents: [CreateModalComponent],
})
export class CreateModalModule { }
