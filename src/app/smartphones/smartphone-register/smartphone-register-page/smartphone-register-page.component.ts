import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbActiveModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { SmartphoneColors, SmartphoneModel, SmartphoneService } from 'src/app/shared';
import { Globals } from 'src/app/shared/globals';
import { SmartphoneFormComponent } from '../smartphone-form/smartphone-form.component';

@Component({
  selector: 'app-smartphone-register-page',
  templateUrl: './smartphone-register-page.component.html',
})
export class SmartphoneRegisterPageComponent implements OnInit {

  smartphone: SmartphoneModel;
  @ViewChild('smartphoneContainerForm') smartphoneForm: SmartphoneFormComponent;

  constructor(
    public globals: Globals,
    public activeModal: NgbActiveModal,
    public config: NgbModalConfig,
    private readonly _route: ActivatedRoute,
    private readonly router: Router,
    private readonly _toastr: ToastrService,
    private readonly _smartphoneService: SmartphoneService

  ) {
    if (!this._route.snapshot.paramMap.get('id')) {
      this.smartphone = new SmartphoneModel({
        color: SmartphoneColors.Black
      });
    } else {
      this.getSmartphoneById(+this._route.snapshot.paramMap.get('id'));
    }
  }

  ngOnInit(): void {

  }

  getSmartphoneById(smartphoneId: number): void {
    this._smartphoneService.getSmartphoneModelsById(smartphoneId)
      .subscribe(([smartphone]) => {
        this.smartphone = Object.assign(new SmartphoneModel(), smartphone, {
          startDate: new Date(smartphone.startDate),
          endDate: new Date(smartphone.endDate)
        });
      });
  }

  goBack() {
    this.router.navigate(['../..']);
  }

  save(): void {
    if (this.smartphone && this.smartphone.id) {
      this.updateSmartphone(this.smartphoneForm.smartphone);
    } else {
      this.createSmartphone(this.smartphoneForm.smartphone);
    }
  }

  updateSmartphone(smartphone: SmartphoneModel): void {
    this._smartphoneService.updateSmartphone(smartphone)
      .subscribe(updated => {
        this.smartphone = updated;
        this._toastr.info('Smartphone atualizado com sucesso.');
        this.router.navigate(['../..']);
      });
  }

  createSmartphone(smartphone: SmartphoneModel): void {
    this._smartphoneService.createSmartphone(smartphone)
      .subscribe(newer => {
        this.smartphone = newer;
        this._toastr.info('Smartphone criado com sucesso.');
        this.router.navigate(['../..']);
      });
  }

}
