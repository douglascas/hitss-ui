import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { SmartphoneItemComponent } from './smartphone-list/smartphone-item/smartphone-item.component';
import { SmartphoneListComponent } from './smartphone-list/smartphone-list.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule
  ],
  exports: [
    SmartphoneListComponent,
    SmartphoneItemComponent
  ],
  declarations: [
    SmartphoneListComponent,
    SmartphoneItemComponent
  ],
})
export class SmartphoneListModule { }

