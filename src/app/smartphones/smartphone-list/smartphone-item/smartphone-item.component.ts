import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { Globals, RegisterType, SmartphoneColors, SmartphoneModel, SmartphoneService } from '../../../shared/index';
import { CreateModalComponent } from '../../smartphone-register';

@Component({
  selector: 'app-smartphone-item',
  templateUrl: './smartphone-item.component.html',
  styleUrls: ['smartphone-item.component.scss']
})
export class SmartphoneItemComponent {

  @Input() smartphone: SmartphoneModel;
  @Output() updateSmartphoneList = new EventEmitter();
  modalRef: NgbModalRef;

  constructor(
    public globals: Globals,
    private readonly _router: Router,
    private readonly _service: SmartphoneService,
    private readonly _toastr: ToastrService,
    private readonly _modalService: NgbModal
  ) { }

  deleteSmartphone(): void {
    this._service.deleteSmartphone(this.smartphone)
      .subscribe(() => {
        this._toastr.success('Smartphone excluído com sucesso!');
        this.updateSmartphoneList.emit();
      });
  }


  editSmartphone(): void {
    if (this.globals.config.configurationRegistrationParams.registerType === RegisterType.Modal) {
      this.openSmartphoneModal();
    } else {
      this.goToDetail();
    }
  }

  openSmartphoneModal(): void {
    this.modalRef = this._modalService.open(CreateModalComponent, { keyboard: false });
    this.modalRef.componentInstance.smartphone = Object.assign(new SmartphoneModel(), this.smartphone);
    this.modalRef.result.then(() => this.updateSmartphoneList.emit());
  }

  goToDetail() {
    this._router.navigate(['/detail/', this.smartphone.id]);
  }

  convertSmarphoneColors(color: SmartphoneColors): string {
    switch (color) {
      case SmartphoneColors.Black:
        return 'Preto';
      case SmartphoneColors.Gold:
        return 'Dourado';
      case SmartphoneColors.Pink:
        return 'Rosa';
      case SmartphoneColors.White:
        return 'Branco';
    }
  }

}
