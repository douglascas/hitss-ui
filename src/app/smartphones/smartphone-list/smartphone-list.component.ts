import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Globals, RegisterType, SmartphoneColors, SmartphoneModel, SmartphoneService } from '../../shared/index';
import { CreateModalComponent } from '../smartphone-register/create-smartphone-modal';

@Component({
  selector: 'app-smartphone-list',
  templateUrl: './smartphone-list.component.html',
  styleUrls: ['./smartphone-list.component.scss']
})
export class SmartphoneListComponent implements OnInit {

  smartphones: SmartphoneModel[] = [];

  smartphone: SmartphoneModel;

  @ViewChild('ngForm')
  ngForm: NgForm;
  modalRef: NgbModalRef;

  constructor(
    public globals: Globals,
    private readonly _service: SmartphoneService,
    private readonly router: Router,
    private readonly _modalService: NgbModal
  ) { }

  ngOnInit() {
    this.getSmartphones();
  }

  getSmartphones(): void {
    this._service.getSmartphones()
      .subscribe(smartphones => {
        this.smartphones = smartphones;
        this.smartphone = new SmartphoneModel();
      });
  }

  registerCellphoneAction(): void {
    if (this.globals.config.configurationRegistrationParams.registerType === RegisterType.Route) {
      this.router.navigate(['/new/']);
    } else {
      this.openSmartphoneModal();
    }
  }

  openSmartphoneModal(): void {
    this.modalRef = this._modalService.open(CreateModalComponent, { keyboard: false });

    if (this.smartphone && this.smartphone.id) {
      this.modalRef.componentInstance.smartphone = this.smartphone;
    } else {
      this.modalRef.componentInstance.smartphone = new SmartphoneModel({
        color: SmartphoneColors.Black
      });
    }
  }

}
