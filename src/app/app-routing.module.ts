import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SmartphoneListComponent } from './smartphones/index';
import { SmartphoneRegisterPageComponent } from './smartphones/smartphone-register/index';

const routes: Routes = [
  {
    path: '',
    component: SmartphoneListComponent,
  },
  {
    path: 'new',
    component: SmartphoneRegisterPageComponent
  },
  {
    path: 'detail/:id',
    component: SmartphoneRegisterPageComponent
  },
  {
    path: '**',
    redirectTo: ''
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
