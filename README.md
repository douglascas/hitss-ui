# Inicialização

Para inicializar o projeto e persistir os dados, é necessário executar:
- O micro-servidor `json-server --watch db.json` para obter os dados no banco no-sql local;
- Inicializar a aplicação com `npm run start`.

# Inicialização
No projeto há, no navbar, um botão para chamada de `configurações`. Essas configurações consistem na exibição de alguns parâmetros durante o teste e para configurar multiplas formas de navegar no sitema (através de rotas ou de dialogs). 
Entendo que não estava nos requisitos, porém preferi implementar para não precisar procurar saídas desnecessárias para implementar uma funcionalidade.

# Considerações
Infelizmente só pude fazer o projeto frontend para entrega no prazo estipulado. 
